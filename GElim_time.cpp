#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <sys/time.h>

typedef unsigned long long u64;

/* get microseconds (us) */
u64 getTime()
{
 struct timeval tv;

 gettimeofday(&tv, NULL);

 u64 ret = tv.tv_usec;

 ret += (tv.tv_sec * 1000 * 1000);

 return ret;
}


using namespace std;

void print_matrix(double** matrix, int rows){

	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < rows+1; ++j)
		{
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

int main(int argc, char *argv[])
{
	int rows = atoi(argv[1]);
	//int rows = ROWS;

	double** matrix, **m_copy;

	matrix = (double**)malloc(rows*sizeof(double*));
	for (int i = 0; i < rows; ++i)
	{
		matrix[i] = (double*)malloc(rows*sizeof(double));
	}

	m_copy = (double**)malloc(rows*sizeof(double*));
	for (int i = 0; i < rows; ++i)
	{
		m_copy[i] = (double*)malloc(rows*sizeof(double));
	}

	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < rows+1; ++j)
		{
			double temp = (rand()%100) + 2;
			matrix[i][j] = temp;
			m_copy[i][j] = temp;
		}
	}

	u64 startTime = getTime();
	for (int col = 0; col < rows-1; ++col)
	{
		for (int row = col+1; row < rows; ++row)
		{			
			double multiplier = matrix[row][col];
			for(int i=col;i<rows+1;i++){
				matrix[row][i] -= (matrix[col][i]*multiplier)/matrix[col][col];
			}
		}
	}
	u64 endTime = getTime();

	double time_taken =  (endTime-startTime)/1000.0f;

	cout << rows << " " << time_taken << endl;

	double x[rows];

	for(int row=rows-1;row >= 0;row--){
		double temp = 0;
		for(int col = row+1;col<rows;col++){
			temp += matrix[row][col]*x[col];
		}
		x[row] = (matrix[row][rows]-temp)/matrix[row][row];
	}

	// Verification
	double calc_b[rows];

	for (int i = 0; i < rows; ++i)
	{
		double sum = 0;
		for (int j = 0; j < rows; ++j)
		{
			sum += m_copy[i][j]*x[j];
		}
		calc_b[i] = sum;
	}

	/*
	for (int i = 0; i < rows; ++i)
	{
		cout << calc_b[i] << " " << m_copy[i][rows] << endl;
	}
	*/

	return 0;
}