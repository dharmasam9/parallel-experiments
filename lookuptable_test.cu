#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <cmath>

#include <iostream>

#include <cuda_runtime.h>
#include "gpu_timer.h"

using namespace std;

/*
Na_m_params = {'A_A':0.1 * (25.0 + EREST_ACT),
                   'A_B': -0.1,
                   'A_C': -1.0,
                   'A_D': -25.0 - EREST_ACT,
                   'A_F':-10.0,
                   'B_A': 4.0,
                   'B_B': 0.0,
                   'B_C': 0.0,
                   'B_D': 0.0 - EREST_ACT,
                   'B_F': 18.0}
Na_h_params = {'A_A': 0.07,
                   'A_B': 0.0,
                   'A_C': 0.0,
                   'A_D': 0.0 - EREST_ACT,
                   'A_F': 20.0,
                   'B_A': 1.0,
                   'B_B': 0.0,
                   'B_C': 1.0,
                   'B_D': -30.0 - EREST_ACT,
                   'B_F': -10.0}
K_n_params = {'A_A': 0.01*(10.0 + EREST_ACT),
                  'A_B': -0.01,
                  'A_C': -1.0,
                  'A_D': -10.0 - EREST_ACT,
                  'A_F': -10.0,
                  'B_A': 0.125,
                  'B_B': 0.0,
                  'B_C': 0.0,
                  'B_D': 0.0 - EREST_ACT,
                  'B_F': 80.0}
*/

__global__
void lookup_alpha(int threadCount, double dx, double* d_lt, double* d_lt_inp, double* d_lt_out){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	if(tid < threadCount){
		int index;
		double frac_part;
		double input = d_lt_inp[tid];

		index = input/dx;
		frac_part = input-(index*dx);
		d_lt_out[tid] = (d_lt[index+1]-d_lt[index])*frac_part/dx + d_lt[index];
	}
		
}

__global__
void calculate_alpha(int threadCount, double* C, double* d_lt_inp, double* d_lt_out){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	if(tid < threadCount)
		d_lt_out[tid] = (C[0] + C[1]*d_lt_inp[tid])/(C[2] + exp((d_lt_inp[tid]+C[3])/C[4]));
		// 0.1*(25 - d_lt_inp[tid])/(exp((25-d_lt_inp[tid])/10) - 1);
}


int main(int argc, char* argv[]){

	//srand(time(NULL));

	int num_divs = 1000;
	int num_lookups = 1000;
	bool ANALYSIS = true;
	//int num_lookups = 10;


	if(argc > 1)
		num_lookups = atoi(argv[1]);

	if(argc > 2)
		ANALYSIS = atoi(argv[2]);



	// Timers
	GpuTimer lt_transfer_timer;
	GpuTimer input_transfer_timer;
	GpuTimer lookup_timer;
	GpuTimer direct_timer;
	GpuTimer output_transfer_timer;

	// Data variables
	double* h_lt, *d_lt;
	double* h_lt_inp, *d_lt_inp;
	double* h_lt_out, *d_lt_out;

	double* h_gate_consts;
	double* d_gate_consts;


	// Data allocation
	h_lt = new double[num_divs]();
	h_lt_inp = new double[num_lookups]();
	h_lt_out = new double[num_lookups]();
	h_gate_consts = new double[5];

	h_gate_consts[0] = 2.5;
	h_gate_consts[1] = -0.1;
	h_gate_consts[2] = -1;
	h_gate_consts[3] = -25;
	h_gate_consts[4] = -10;

	cudaMalloc((void**)&d_lt, num_divs* sizeof(double));
	cudaMalloc((void**)&d_lt_inp, num_lookups* sizeof(double));
	cudaMalloc((void**)&d_lt_out, num_lookups* sizeof(double));
	cudaMalloc((void**)&d_gate_consts, 5* sizeof(double));


	// Generate lookup table
	double MIN = 0;
	double MAX = 100;
	double dx = (MAX-MIN)/num_divs;

	for (int i = 0; i < num_divs; ++i)
		h_lt[i] = 0.1*(25 - i*dx)/(exp((25-i*dx)/10) - 1);	

	// Generate random lookup inputs
	for (int i = 0; i < num_lookups; ++i)
		h_lt_inp[i] = rand()%((int)(MAX-MIN));


	// Copy LT, and lookups to GPU
	lt_transfer_timer.Start();
		cudaMemcpy(d_lt, h_lt, num_divs*sizeof(double), cudaMemcpyHostToDevice);
		cudaMemcpy(d_gate_consts, h_gate_consts, 5*sizeof(double), cudaMemcpyHostToDevice);
	lt_transfer_timer.Stop();

	input_transfer_timer.Start();
		cudaMemcpy(d_lt_inp, h_lt_inp, num_lookups*sizeof(double), cudaMemcpyHostToDevice);
	input_transfer_timer.Stop();
	 

	// Perform CPU lookups

	clock_t begin = clock();
		int index;
		double frac_part;

		for (int i = 0; i < num_lookups; ++i)
		{
			index = h_lt_inp[i]/dx;
			frac_part = h_lt_inp[i]-(index*dx);
			h_lt_out[i] = (h_lt[index+1]-h_lt[index])*frac_part/dx + h_lt[index];

			/*
			cout << h_lt_out[i] << " ";
			if(i==num_lookups-1) cout << endl;
			*/
		}

	clock_t end = clock();
	double cpu_time = (double(end - begin)*1000) / CLOCKS_PER_SEC;

	// Perform GPU lookups
	int THREADS_PER_BLOCK = 256;
	int BLOCKS = ceil((num_lookups*1.0)/THREADS_PER_BLOCK);

	lookup_timer.Start();
		lookup_alpha<<<BLOCKS,THREADS_PER_BLOCK>>>(num_lookups , dx, d_lt, d_lt_inp, d_lt_out);
		cudaDeviceSynchronize();
	lookup_timer.Stop();

	direct_timer.Start();
		calculate_alpha<<<BLOCKS,THREADS_PER_BLOCK>>>(num_lookups , d_gate_consts, d_lt_inp, d_lt_out);
		cudaDeviceSynchronize();
	direct_timer.Stop();

	//Transferring back to CPU
	output_transfer_timer.Start();
		cudaMemcpy(h_lt_out, d_lt_out, num_lookups*sizeof(double), cudaMemcpyDeviceToHost);
	output_transfer_timer.Stop();


	if(ANALYSIS){
		// #comp, cpu_time, gpu_lookup_time, gpu_direct_time, mem_trans_time, lookup_kernel, direct_kernel
		double gpu_memtrans_time = input_transfer_timer.Elapsed() + output_transfer_timer.Elapsed();
		double gpu_lt_time = gpu_memtrans_time + lookup_timer.Elapsed();
		double gpu_direct_time = gpu_memtrans_time + direct_timer.Elapsed();
		double speedup = lookup_timer.Elapsed()/direct_timer.Elapsed();

		cout << num_lookups << "," << cpu_time << ","
			<< gpu_lt_time << "," << gpu_direct_time << ","
			<< gpu_memtrans_time << ","
			<< lookup_timer.Elapsed() << "," << direct_timer.Elapsed() ","
			<< speedup <<  endl;
	}else{
		cout << (lt_transfer_timer.Elapsed() + input_transfer_timer.Elapsed()) << " (" << lt_transfer_timer.Elapsed() << "," << input_transfer_timer.Elapsed() << ")" <<endl;
		cout << fixed << cpu_time <<  " "  << lookup_timer.Elapsed() << " " << direct_timer.Elapsed() << endl;
		cout << output_transfer_timer.Elapsed()	 << endl;	
	}

	/*
	for (int i = 0; i < num_lookups; ++i){
		cout << fixed << h_lt_out[i] << " ";
		if(i==num_lookups-1) cout << endl;	
	}
	*/
	


	// Timings

return 0;
}
