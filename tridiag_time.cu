// Time plot for tridiagonal solver.

#include <cuda_runtime.h>
#include <cusparse_v2.h>

#include "Gpu_timer.h"

#include <cstdlib>
#include <cstdio>
#include <sys/time.h>

#include <iostream>

#define RAND (rand()%100 + 1)/100.0

using namespace std;

using namespace std;

typedef unsigned long long u64;

/* get microseconds (us) */
u64 getTime()
{
 struct timeval tv;

 gettimeofday(&tv, NULL);

 u64 ret = tv.tv_usec;

 ret += (tv.tv_sec * 1000 * 1000);

 return ret;
}

double tridiagsolver(int N){
	int array_size = 3*N;
	double* values = new double[array_size];
	double* values_copy = new double[array_size];
	double* b = new double[N];
	double* x = new double[N];

	double gem_time = 0;

	memcpy(values_copy, values, array_size*sizeof(double));

	for(int i=0;i<array_size;i++){
		values[i] = rand()%20+1;
	}

	for(int i=0;i<N;i++){
		b[i] = (rand()%20)+1;
	}

	values[0] = 0;
	values[array_size-1] = 0;

	u64 start = getTime();
		// Gauss elimination
		// Forward elimination
		for(int i=1;i<N;i++){
			values[3*i+1] -= (values[3*(i-1)+2] * values[3*i]) / values[3*(i-1)+1];
		}

		// Back substitution
		x[N-1] = values[3*(N-1)+1];

		for(int i=N-2;i>=0;i--){
			x[i] = (b[i] - values[3*i+2]*x[i+1])/values[3*i+1];
		}
	u64 end = getTime();

	gem_time = (end-start)/1000.0f;

	//cout << "Time taken is " << gem_time << endl;

	// Verification
	double* calc_b = new double[N];
	for(int i=0;i<N;i++){
		if(i==0){
			calc_b[i] = values[3*i+1]*x[i] + values[3*i+2]*x[i+1];
		}else if(i == N-1){
			calc_b[i] = values[3*i]*x[i-1] + values[3*i+1]*x[i];
		}else{
			calc_b[i] = values[3*i]*x[i-1] + values[3*i+1]*x[i] + values[3*i+2]*x[i+1];
		}
	}

	double error = 0;
	for(int i=0;i<N;i++)
		error += (calc_b[i]-b[i]);

	//cout <<  "error is " << error << endl;

	return gem_time;

}


int main(int argc, char **argv) {
	double* h_tridiag_data;
	double* d_tridiag_data;

	double* h_b, *d_b;

	int start_size = 1024;
	int repeats = 30;

	// Creating cusparse handle
	cusparseHandle_t cusparseH = NULL;
	cusparseCreate(&cusparseH);


	for (int size = start_size; size <= 256*start_size; size=2*size)
	{
		// Allocating memory
		h_tridiag_data = new double[3*size]();
		h_b = new double[size]();
		cudaMalloc((void**)&d_tridiag_data, 3*size*sizeof(double));
		cudaMalloc((void**)&d_b, size*sizeof(double));

		// Populating data
		for(int i=0;i<size;i++){
			h_tridiag_data[i] = RAND;
			h_tridiag_data[2*size+i] = RAND;
			h_tridiag_data[size+i] = h_tridiag_data[i] + h_tridiag_data[2*size+i];

			h_b[i] = RAND;
		}
		h_tridiag_data[0] = 0;
		h_tridiag_data[3*size-1] = 0;

		// Transfering data to GPU
		cudaMemcpy(d_tridiag_data, h_tridiag_data, 3*size*sizeof(double), cudaMemcpyHostToDevice);

		float cum_gpu_time = 0;
		float cum_cpu_time = 0;

		for(int i=0;i<repeats;i++){
			cudaMemcpy(d_b, h_b, size*sizeof(double), cudaMemcpyHostToDevice);

			GpuTimer tridiagTimer;

			// Solving on GPU
			tridiagTimer.Start();
				cusparseDgtsv_nopivot(cusparseH, size, 1, &(d_tridiag_data[0]), &(d_tridiag_data[size]), &(d_tridiag_data[2*size]), d_b, size);
				cudaDeviceSynchronize();
			tridiagTimer.Stop();

			cum_gpu_time += tridiagTimer.Elapsed();
			cum_cpu_time += tridiagsolver(size);
		}

		// Freeing memory
		cudaFree(d_tridiag_data);
		cudaFree(d_b);
		delete h_tridiag_data;
		delete h_b;

		cout << size << "," << cum_gpu_time/repeats << "," << cum_cpu_time/repeats << endl;
	}


	return 0;
}
