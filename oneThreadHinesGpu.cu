#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

#include <cstdlib>
#include <cstdio>
#include <assert.h>
#include <sys/time.h>

#include <cusparse_v2.h>
#include "Gpu_timer.h"

using namespace std;

void print_tridiagonal_as_matrix(double* data,int rows){
    cout << setprecision(3);

    double zero = 0;

    int zeros_left = -1;
    int zeros_right = rows-2;

    string delimeter = " , ";

    cout << "[" << endl;
    for(int i=0;i<rows;i++){

        for(int j=0;j<zeros_left;j++){
            cout << zero << delimeter;
        }

        if(i != 0)
            cout << data[i] << delimeter;

        cout << data[rows+i] << delimeter ;

        if(i != rows-1)
            cout << data[2*rows+i] << delimeter;

        for(int j=0;j<zeros_right;j++){
            cout << zero << delimeter;
        }

        zeros_left++;
        zeros_right--;

        cout << ";" << endl;
    }
    cout << "]" << endl;

}


template <typename T>
void print_matrix(T* data, int rows, int columns){
    cout << setprecision(3);
    cout << "[" << endl;;
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < columns; ++j)
        {
            cout << data[rows*j + i] << ", ";
        }
        cout << ";" <<endl;
    }
    cout << "]" << endl;
}



__global__
void solveHines(int num_comp, int tri_size, double* left, double* main, double* right, double* rhs){

    int tid = blockIdx.x*blockDim.x + threadIdx.x;
    int offset = tid*tri_size;

    // Forward elimination
    for (int i = 1; i < tri_size; ++i)
    {

        main[offset+i] -= right[offset+i-1]*(left[offset+i]/main[offset+i-1]);
        rhs[offset+i] -= rhs[offset+i-1]*(left[offset+i]/main[offset+i-1]);
    }


    // Back Substitution
    rhs[offset+tri_size-1] /= main[offset+tri_size-1];
    for (int i = tri_size-2; i >=0; i--)
    {
        rhs[offset+i] =  (rhs[offset+i] -  right[offset+i]*rhs[offset+i+1])/main[offset+i];
    }


}


int main(int argc, char* argv[]){

    int lin_rows = atoi(argv[1]);

    double* h_linear_tridiag;
    double* h_linear_x;
    double* h_linear_rhs;

    h_linear_tridiag = new double[3*lin_rows]();
    h_linear_rhs = new double[lin_rows]();
    h_linear_x = new double[lin_rows]();

    for(int i=0;i<3*lin_rows;i++)
        h_linear_tridiag[i] = rand()%1000+2;

    for(int i=0;i<lin_rows;i++)
        h_linear_rhs[i] = rand()%1000+2;


   double* d_lin_tridiag, *d_lin_rhs, *d_lin_x;

    cudaMalloc((void**)&d_lin_tridiag, 3*lin_rows*sizeof(double));
    cudaMalloc((void**)&d_lin_rhs, lin_rows*sizeof(double));
    cudaMallocManaged((void**)&d_lin_x, lin_rows*sizeof(double));

    // Transferring linear solver data
    cudaMemcpy(d_lin_tridiag, h_linear_tridiag, 3*lin_rows*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_lin_rhs, h_linear_rhs, lin_rows*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_lin_x, h_linear_rhs, lin_rows*sizeof(double), cudaMemcpyHostToDevice);
    
    GpuTimer timer;
    int smallSize = 10;
    timer.Start();
    solveHines<<<15,192>>>(lin_rows, smallSize, d_lin_tridiag, &d_lin_tridiag[lin_rows],  &d_lin_tridiag[2*lin_rows], d_lin_rhs);
    timer.Stop();

    cudaMemcpy(h_linear_x, d_lin_rhs, lin_rows*sizeof(double), cudaMemcpyDeviceToHost);
    
    /*
    h_linear_tridiag[2*lin_rows+smallSize-1] = 0;
    h_linear_tridiag[smallSize] = 0;

    print_tridiagonal_as_matrix(h_linear_tridiag, lin_rows);
    print_matrix(h_linear_rhs, lin_rows,1);  
    print_matrix(h_linear_x, lin_rows,1);
    */
    

    cout << lin_rows << " " << timer.Elapsed() << endl;

return 0;
}
