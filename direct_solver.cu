#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>


#include <algorithm>
#include <fstream>
#include <sstream>

#include <cusolverSp.h>
#include <cuda_runtime.h>
#include <cusolverSp_LOWLEVEL_PREVIEW.h>

#include <cublas_v2.h>
#include <thrust/reduce.h>
#include <thrust/device_ptr.h>

#include "Gpu_timer.h"

using namespace std;

// Read from .swc file
// Generate the matrix with random values
// Make it diagonally dominant.
// Solve using cusparse direct method

void fill_matrix_using_junction(int num_comp, const vector<vector<int> > &junction_list,
	vector<double> &h_values, vector<int> &h_colInd, vector<int> &h_rowPtr, vector<int> &h_endPtr, vector<double> & h_b,
	vector<double> &h_maindiag_passive, vector<double> &h_Ga){
	int DEBUG = 0;
	int FILL_RAND = true;

	// Printing junction data
	if(DEBUG){
		for (int i = 0; i < junction_list.size(); ++i)
		{
			cout << i << " -> ";
			for (int j = 0; j < junction_list[i].size(); ++j)
			{
				cout << junction_list[i][j] << ",";
			}
			cout << endl;
		}
	}

	// Populating passive part of main diagonal
	for(int i=0;i<num_comp;i++)
		if(FILL_RAND)
			h_maindiag_passive[i] = rand()%10+2;
		else
			h_maindiag_passive[i] = 5;

	// Populating conductance
	for(int i=0;i<num_comp;i++)
		if(FILL_RAND)
			h_Ga[i] = (rand()%10)+2;
		else
			h_Ga[i] = 5;


	// Non zero elements in csr format.
	vector<pair<int,double> > non_zero_elements;
	int node1,node2;
	double gi,gj,gij;
	double junction_sum;

	// Generating symmetrix admittance graph
	// using junction information.
	for (int i = 0; i < num_comp; ++i)
	{
		if(junction_list[i].size() > 1){

			// Calculating junction sum
			junction_sum = 0;
			for (int k = 0; k < junction_list[i].size(); ++k)
				junction_sum += h_Ga[junction_list[i][k]];


			// Putting admittance in off diagonal elements.
			for (int j = 0; j < junction_list[i].size(); ++j)
			{
				node1 = junction_list[i][j];

				// Inducing passive effect to main diagonal elements
				h_maindiag_passive[node1] += h_Ga[node1]*(1.0 - h_Ga[node1]/junction_sum);

				for (int k = j+1; k < junction_list[i].size(); ++k)
				{
					node2 = junction_list[i][k];

					gi = h_Ga[node1];
					gj = h_Ga[node2];
					gij = (gi*gj)/junction_sum;

					//cout << junction_sum << " " << gi[node1] << " " << gi[node2] << " " << admittance << endl;

					// Pushing element and its symmetry.
					non_zero_elements.push_back(make_pair(node1*num_comp+node2, -1*gij));
					non_zero_elements.push_back(make_pair(node2*num_comp+node1, -1*gij));
				}
			}

		}
	}


	// Add main diagonal to non_zero_elements.
	for (int i = 0; i < num_comp; ++i){
		non_zero_elements.push_back(make_pair(i*num_comp+i, h_maindiag_passive[i]));
	}

	// Initializing a cusp csr matrix
	int nnz = non_zero_elements.size();

	h_values.resize(nnz);
	h_colInd.resize(nnz);

	// Getting elements in csr format.
	// and populating tri diagonal
	sort(non_zero_elements.begin(), non_zero_elements.end());

	int r,c;
	double value;
	for (int i = 0; i < nnz; ++i)
	{
		r = non_zero_elements[i].first/num_comp;
		c = non_zero_elements[i].first%num_comp;
		value = non_zero_elements[i].second;

		h_rowPtr[r]++;
		h_colInd[i] = c;
		h_values[i] = value;
	}

	int temp;
	int sum = 0;
	// Scan operation on rowPtr;
	for (int i = 0; i < num_comp+1; ++i)
	{
		temp = h_rowPtr[i];
		h_rowPtr[i] = sum;
		sum += temp;
		h_endPtr[i] = sum;
	}

	// Populating rhs to zero
	for (int i = 0; i < num_comp; ++i)
		if(FILL_RAND)
			h_b[i] = (rand()%10) + 1;
		else
			h_b[i] = 5;

}


int get_rows_from_file(char* file_name){
	ifstream input_file(file_name);
	string line;

	int rows = 0;
	while(getline(input_file, line)){
		if(line[0] != '#')
			rows++;
	}

	input_file.ignore();
	input_file.close();


	return rows;

}

void get_structure_from_neuron(char* file_name, int num_comp, vector< vector<int> > &junction_list){

	// Initializing junctions.
	for (int i = 0; i < num_comp; ++i)
		junction_list[i].push_back(i);

	// read from file and get junction data.
	ifstream input_file(file_name);
	string line;

	int parent,child;
	double temp_double;
	while(getline(input_file, line)){
		if(line[0] != '#'){
			stringstream ss(line);
			ss >> child;
			ss >> parent;
			ss >> temp_double;
			ss >> temp_double;
			ss >> temp_double;
			ss >> temp_double;
			ss >> parent;

			if(parent != -1){
				junction_list[parent-1].push_back(child-1);
			}

			//cout << child << " " << parent << endl;
		}
	}

	input_file.close();
}


void print_matrix(vector<double> &h_values, vector<double> &h_b, vector<int> &h_colInd, vector<int> &h_rowPtr, vector<int> &h_endPtr){
	int num_comp = h_rowPtr.size()-1;
	int nnz = h_values.size();
	double zero = 0;

	for (int i = 0; i < num_comp; ++i)
	{
		int j = 0;
		int k = h_rowPtr[i];

		while(h_colInd[k]<h_endPtr[i] && j<num_comp){
			if(h_colInd[k]==j){
				printf("%7.2f ",h_values[k]);
				k++;
			}else{
				printf("%7.2f ",zero);
			}
			j++;
		}

		while(j < num_comp)
			cout << zero << " ";
		cout << endl;
	}
	cout << endl;

	for(int i=0;i<num_comp;i++)
		cout << h_b[i] << endl;

}



int main(int argc, char *argv[])
{
	srand(time(NULL));

	int num_comp;
	vector<vector<int> > junction_list;

	// Matrix
	vector<double> h_values;
	vector<int> h_colInd;
	vector<int> h_rowPtr;
	vector<int> h_endPtr;

	vector<double> h_b;
	vector<double> h_maindiag_passive;

	double* h_tridiag_data;

	// Values
	vector<double> h_Ga;

	char* fn = argv[1];

	num_comp = get_rows_from_file(fn);
	junction_list.resize(num_comp);
	get_structure_from_neuron(fn, num_comp, junction_list);

	// Allocating some matrix elements
	h_b.resize(num_comp);
	h_maindiag_passive.resize(num_comp,0);
	h_rowPtr.resize(num_comp+1,0);
	h_endPtr.resize(num_comp+1,0);
	h_tridiag_data = new double[3*num_comp]();

	h_Ga.resize(num_comp);

	fill_matrix_using_junction(num_comp, junction_list,
								h_values, h_colInd, h_rowPtr, h_endPtr, h_b, h_maindiag_passive, h_Ga);

	// Separate tridiagonal data.
	int dia_num;
	int r,c;
	for(int i=0;i<num_comp;i++){
		r = i;
		for(int j=h_rowPtr[i];j<h_rowPtr[i+1];j++){
			c = h_colInd[j];
			dia_num = c-r;
			switch(dia_num){
			case -1:
				h_tridiag_data[i] = h_values[j];
				break;
			case 0:
				h_tridiag_data[num_comp + i] = h_values[j];
				break;
			case 1:
				h_tridiag_data[2*num_comp + i] = h_values[j];
				break;
			}
		}
	}


	// Printing matrix
	if(num_comp < 10){
		for(int i=0;i<num_comp+1;i++){
			//cout << h_rowPtr[i] << " " << h_endPtr[i] << endl;
		}

		print_matrix(h_values, h_b, h_colInd, h_rowPtr, h_endPtr);
		/*
		for(int i=0;i<num_comp;i++){
			cout << h_tridiag_data[i] << " " << h_tridiag_data[num_comp+i] << " " << h_tridiag_data[2*num_comp+i] << endl;
		}
		*/
	}

	// Initializing device array
	double* d_values;
	double* d_x, *d_y;
	double* d_b, *d_calc_b;
	double* d_tridiag_data;
	int* d_colInd;
	int* d_rowPtr;
	int* d_endPtr;

	// LU factorization
	double* d_valsILU0;

	int nnz = h_values.size();

	cudaMalloc((void**)&d_values, nnz*sizeof(double));
	cudaMalloc((void**)&d_x, num_comp*sizeof(double));
	cudaMalloc((void**)&d_b, num_comp*sizeof(double));
	cudaMalloc((void**)&d_calc_b, num_comp*sizeof(double));
	cudaMalloc((void**)&d_tridiag_data, 3*num_comp*sizeof(double));

	cudaMalloc((void**)&d_valsILU0, nnz*sizeof(double));
	cudaMalloc((void**)&d_y, num_comp*sizeof(double));

	cudaMalloc((void**)&d_colInd, nnz*sizeof(int));
	cudaMalloc((void**)&d_rowPtr, (num_comp+1)*sizeof(int));
	cudaMalloc((void**)&d_endPtr, (num_comp+1)*sizeof(int));

	cudaMemset(d_x, 0,  num_comp*sizeof(double));

	cudaMemcpy(d_values, &(h_values[0]), nnz*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, &(h_b[0]), num_comp*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_colInd, &(h_colInd[0]), nnz*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_rowPtr, &(h_rowPtr[0]), (num_comp+1)*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_endPtr, &(h_endPtr[0]), (num_comp+1)*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_tridiag_data, h_tridiag_data, 3*num_comp*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_valsILU0, d_values, nnz*sizeof(double), cudaMemcpyDeviceToDevice);

	// ---------------- Using cusolverSp Low level function ----------------------------

	double* h_val = new double[nnz];
	double* h_rhs = new double[num_comp];
	int* h_col = new int[nnz];
	int* h_row = new int[num_comp+1];

	for(int i=0;i<nnz;i++) {
		h_val[i] = h_values[i];
		h_col[i] = h_colInd[i];
	}

	for(int i=0;i<num_comp+1;i++){
		h_row[i] = h_rowPtr[i];
		if(i<num_comp) h_rhs[i] = h_b[i];
	}


	csrluInfoHost_t infoA;
	cusolverSpHandle_t cusolverH;
	cusparseMatDescr_t descrA = 0;
	cusolverStatus_t cusolver_status;

	// Creating matrix description
	cusparseCreateMatDescr(&descrA);
	cusparseSetMatType(descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrA, CUSPARSE_INDEX_BASE_ZERO);

 	cusolver_status =  cusolverSpCreateCsrluInfoHost(&infoA); // Creating info of A
 	cout << "Creating info " << cusolver_status << endl;
 	cusolver_status = cusolverSpCreate(&cusolverH); // Creating cusolver handle
	cout << "Creating handler " << cusolver_status << endl;

	cusolver_status = cusolverSpXcsrluAnalysisHost(cusolverH, num_comp, nnz, descrA, h_row, h_col, infoA);
	cout << "Performing analysis " << cusolver_status << endl;

	size_t internalDataInBytes;
	size_t workspaceInBytes;

	double pivot_thresh = 0;
	double h_x[num_comp];

	cusolver_status = cusolverSpDcsrluBufferInfoHost(cusolverH, num_comp, nnz, descrA, h_val, h_row, h_col,
			infoA, &internalDataInBytes, &workspaceInBytes);
	cout << "Gettting buffer info " << cusolver_status << endl;

	// Allocate memory
	cout << internalDataInBytes << " " << workspaceInBytes << endl;
	double* internalBuffer = (double*) malloc(internalDataInBytes);
	double* workspaceBuffer = (double*) malloc(workspaceInBytes);

	GpuTimer solverTimer;

	solverTimer.Start();
	cusolver_status = cusolverSpDcsrluFactorHost(cusolverH, num_comp, nnz, descrA, h_val, h_row, h_col,
			infoA, pivot_thresh ,workspaceBuffer);
	//cout << "Factorization " << cusolver_status << endl;

	cusolver_status = cusolverSpDcsrluSolveHost(cusolverH, num_comp, &(h_b[0]), h_x, infoA, internalBuffer);
	solverTimer.Stop();

	cout << "Time taken " << solverTimer.Elapsed() << endl;

	if(num_comp < 10)
		for(int i=0;i<num_comp;i++)
			cout << h_x[i] << endl;

/*
	// ---------------- Using cusolverSp higher  host function ------------------------------.
	/*
	cusolverSpHandle_t cusolverH = NULL;
	cusparseHandle_t cusparseH = NULL;

	cusparseMatDescr_t descrA = 0;
	cusparseCreateMatDescr(&descrA);
	cusparseSetMatType(descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrA, CUSPARSE_INDEX_BASE_ZERO);

    cusolverStatus_t cusolver_status = cusolverSpCreate(&cusolverH);
    cusparseStatus_t cusparse_status = cusparseCreate(&cusparseH);
    double tol = 0.1;
    int reorder = 0;
    int singularity;

    // Using Cusparse
	const double alpha = 1.0;
	const double beta = 0.0;

    double* h_x = new double[num_comp]();
    GpuTimer solverTimer, tridiagTimer, memoryTimer;
    solverTimer.Start();

    cusolver_status = cusolverSpDcsrlsvluHost(cusolverH,
        		num_comp,
        		nnz,
        		descrA,
        		&(h_values[0]),
        		&(h_rowPtr[0]),
        		&(h_colInd[0]),
        		&(h_b.front()),
        		tol,reorder,
        		h_x,&singularity);


    //cusolver_status = cusolverSpDcsrlsvchol(cusolverH, num_comp, nnz, descrA, d_values, d_rowPtr, d_colInd, d_b, tol, reorder, d_x, &singularity);

    solverTimer.Stop();

    cudaMemcpy(d_x, h_x, num_comp*sizeof(double), cudaMemcpyHostToDevice);
    cout << "LU time: " << solverTimer.Elapsed() << endl;

    cout << "Singularity is " << singularity << endl;
    cout << "status of call " << cusolver_status << endl;

	// Finding error
    cusparseDcsrmv(cusparseH,  CUSPARSE_OPERATION_NON_TRANSPOSE,
    		num_comp, num_comp, nnz, &alpha, descrA,
    		d_values, d_rowPtr, d_colInd,
    		d_x , &beta, d_calc_b);

    double h_calc_b[num_comp];
    cudaMemcpy(h_calc_b, d_calc_b, num_comp*sizeof(double), cudaMemcpyDeviceToHost);

    double error = 0;
    for(int i=0;i<num_comp;i++){
    	error += (h_calc_b[i] - h_b[i]);
    }

    cout << "Error is " << error << endl;

    cusolverSpDestroy(cusolverH);
    cusparseDestroy(cusparseH);
    */

	/*
	// -------------------------- LU followed by triangular solver -------------------------------------

	GpuTimer solverTimer;

	// Cusparse handle
	cusparseHandle_t cusparseH;
	cusparseCreate(&cusparseH);
	cusparseStatus_t cusparseStatus;

	// Matrix descriptin
	cusparseMatDescr_t descrA = 0;
	cusparseCreateMatDescr(&descrA);
	cusparseSetMatType(descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrA, CUSPARSE_INDEX_BASE_ZERO);

	// Cusparse Analaysis structure
	cusparseSolveAnalysisInfo_t infoA;
	cusparseCreateSolveAnalysisInfo(&infoA);

	// Create info objects for the ILU0 preconditioner
	cusparseSolveAnalysisInfo_t info_u;
	cusparseCreateSolveAnalysisInfo(&info_u);

	cusparseMatDescr_t descrL = 0;
	cusparseStatus = cusparseCreateMatDescr(&descrL);
	cusparseSetMatType(descrL,CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrL,CUSPARSE_INDEX_BASE_ZERO);
	cusparseSetMatFillMode(descrL, CUSPARSE_FILL_MODE_LOWER);
	cusparseSetMatDiagType(descrL, CUSPARSE_DIAG_TYPE_UNIT);

	cusparseMatDescr_t descrU = 0;
	cusparseStatus = cusparseCreateMatDescr(&descrU);
	cusparseSetMatType(descrU,CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrU,CUSPARSE_INDEX_BASE_ZERO);
	cusparseSetMatFillMode(descrU, CUSPARSE_FILL_MODE_UPPER);
	cusparseSetMatDiagType(descrU, CUSPARSE_DIAG_TYPE_NON_UNIT);

	cusparseDcsrsv_analysis(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE,
				num_comp, nnz, descrA, d_values, d_rowPtr, d_colInd, infoA);

	cusparseDcsrsv_analysis(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE,
				num_comp, nnz, descrU, d_values, d_rowPtr, d_colInd, info_u);

	double alpha = 1;

	solverTimer.Start();

	cusparseDcsrilu0(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE, num_comp, descrA, d_valsILU0, d_rowPtr, d_colInd, infoA);

	cusparseDcsrsv_solve(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE,
			num_comp, &alpha, descrL, d_valsILU0, d_rowPtr, d_colInd, infoA, d_b, d_y);

	cusparseDcsrsv_solve(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE,
				num_comp, &alpha, descrU, d_valsILU0, d_rowPtr, d_colInd, info_u, d_y, d_x);

	solverTimer.Stop();

	cout << fixed << "Time taken: " << solverTimer.Elapsed() << endl;

	double h_x[num_comp];
	cudaMemcpy(h_x, d_x, num_comp*sizeof(double), cudaMemcpyDeviceToHost);

	if(num_comp < 10)
		for(int i=0;i<num_comp;i++)
			cout << h_x[i] << endl;

	// Using analysis for other matrix of same structure
	GpuTimer solver2Timer;

	// Allocating some matrix elements
	h_rowPtr.clear();
	h_endPtr.clear();

	h_rowPtr.resize(num_comp+1,0);
	h_endPtr.resize(num_comp+1,0);

	h_values.clear();
	h_colInd.clear();

	fill_matrix_using_junction(num_comp, junction_list,
									h_values, h_colInd, h_rowPtr, h_endPtr, h_b, h_maindiag_passive, h_Ga);

	if(num_comp < 10){
		for(int i=0;i<num_comp+1;i++){
		//	cout << h_rowPtr[i] << " " << h_endPtr[i] << endl;
		}

		print_matrix(h_values, h_b, h_colInd, h_rowPtr, h_endPtr);

		for(int i=0;i<num_comp;i++){
			// cout << h_tridiag_data[i] << " " << h_tridiag_data[num_comp+i] << " " << h_tridiag_data[2*num_comp+i] << endl;
		}


	}

	cudaMemset(d_x, 0,  num_comp*sizeof(double));

	cudaMemcpy(d_values, &(h_values[0]), nnz*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, &(h_b[0]), num_comp*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_colInd, &(h_colInd[0]), nnz*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_rowPtr, &(h_rowPtr[0]), (num_comp+1)*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_endPtr, &(h_endPtr[0]), (num_comp+1)*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_tridiag_data, h_tridiag_data, 3*num_comp*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_valsILU0, d_values, nnz*sizeof(double), cudaMemcpyDeviceToDevice);

	solver2Timer.Start();

	cusparseDcsrilu0(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE, num_comp, descrA, d_valsILU0, d_rowPtr, d_colInd, infoA);

	cusparseDcsrsv_solve(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE,
			num_comp, &alpha, descrL, d_valsILU0, d_rowPtr, d_colInd, infoA, d_b, d_y);

	cusparseDcsrsv_solve(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE,
				num_comp, &alpha, descrU, d_valsILU0, d_rowPtr, d_colInd, info_u, d_y, d_x);

	solver2Timer.Stop();

	cudaMemcpy(h_x, d_x, num_comp*sizeof(double), cudaMemcpyDeviceToHost);

	if(num_comp < 10)
		for(int i=0;i<num_comp;i++)
			cout << h_x[i] << endl;

	double beta = 0;
	cusparseDcsrmv(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE, num_comp, num_comp, nnz, &alpha, descrA, d_values,
			d_rowPtr, d_colInd, d_x, &beta, d_y);

	cublasHandle_t cublasH;
	cublasCreate_v2(&cublasH);

	double alpha_minus = -1;
	cublasDaxpy(cublasH, num_comp, &alpha_minus , d_y, 1, d_b, 1);

	thrust::device_ptr<double> thrust_d_b(d_b);

	double r1 = thrust::reduce(thrust_d_b, thrust_d_b+num_comp);

	cout << "Errr " << r1 << endl;

	cout << "Second TIme " << solver2Timer.Elapsed() << endl;
	*/


	return 0;
}
