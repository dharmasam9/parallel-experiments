#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <sys/time.h>
#include <math.h>
#include <omp.h>

#include <cusparse_v2.h>
#include "Gpu_timer.h"

using namespace std;

void fill_array_with_randnumbers(double* data, int size){
	for(int i=0;i<size;i++){
		data[i] = rand()%50 + 2;
	}
}

void fill_diagonally_dominant_tridiagonal(double* data, int size){
	fill_array_with_randnumbers(data, size);
	fill_array_with_randnumbers(&data[size], size);

	// Making it symmetric
	for(int i=1;i<size;i++){
		 data[2*size+i-1] = data[i];
	}

	// Making boundaries zero
	data[0] = 0;
	data[3*size-1] = 0;

	// Making it diagonally dominant
	for(int i=0;i<size;i++){
		data[size+i] += (data[i] + data[2*size+i]);
	}
}

void print_tridiagonal_system(double* data, double* rhs, int rows){
	double zero = 0;
	int zeros_left = -1;
	int zeros_right = rows-2;

	string delimeter = " , ";

	cout << "[" << endl;
	for(int i=0;i<rows;i++){

		for(int j=0;j<zeros_left;j++){
			cout << zero << delimeter;
		}

		if(i != 0)
			cout << data[i] << delimeter;

		cout << data[rows+i] << delimeter ;

		if(i != rows-1)
			cout << data[2*rows+i] << delimeter;

		for(int j=0;j<zeros_right;j++){
			cout << zero << delimeter;
		}

		zeros_left++;
		zeros_right--;

		cout << rhs[i] << ";" << endl;
	}
	cout << "]" << endl;
}

template <typename T>
void print_matrix(T* data, int rows, int columns){
	cout << setprecision(3);
	cout << "[" << endl;;
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < columns; ++j)
		{
			cout << data[rows*j + i] << ", ";
		}
		cout << ";" <<endl;
	}
	cout << "]" << endl;
}


int main(int argc, char const *argv[])
{
	int size = atoi(argv[1]);
	int num_rhs = atoi(argv[2]);
	int lin_size = num_rhs*size;

	double* data = new double[3*size]();
	double* rhs_many = new double[num_rhs*size]();

	double* lin_data = new double[3*lin_size]();

	fill_diagonally_dominant_tridiagonal(data, size);
	fill_array_with_randnumbers(rhs_many, num_rhs*size);

	// Forming linear data.
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			for (int k = 0; k < num_rhs; ++k)
			{
				lin_data[j*lin_size + k*size + i] = data[j*size + i];
			}
		}
	}

	//print_tridiagonal_system(data, rhs_many, size);
	//print_tridiagonal_system(lin_data, rhs_many, lin_size);

	double* d_data;
	double* d_lin_data;
	double* d_rhs_many, *d_rhs_lin;

	cudaMalloc((void**)&d_data, 3*size*sizeof(double));
	cudaMalloc((void**)&d_lin_data, 3*lin_size*sizeof(double));
	cudaMallocManaged((void**)&d_rhs_many, num_rhs*size*sizeof(double));
	cudaMallocManaged((void**)&d_rhs_lin, lin_size*sizeof(double));

	cudaMemcpy(d_data, data, 3*size*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_rhs_many, rhs_many, num_rhs*size*sizeof(double), cudaMemcpyHostToDevice);

	cudaMemcpy(d_lin_data, lin_data, 3*lin_size*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_rhs_lin, rhs_many, lin_size*sizeof(double), cudaMemcpyHostToDevice);
	
	// Cusparse
	cusparseHandle_t cusparseH;
	cusparseMatDescr_t descrA;

	// Initializing variables
	cusparseCreate(&cusparseH);

	cusparseCreateMatDescr(&descrA);
	cusparseSetMatType(descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrA, CUSPARSE_INDEX_BASE_ZERO);


	GpuTimer tridiag_manyrhs_Timer, tridiag_linear_timer;

	tridiag_linear_timer.Start();
		cusparseDgtsv_nopivot(cusparseH, lin_size, 1,
				&d_lin_data[0], &d_lin_data[lin_size], &d_lin_data[2*lin_size],
				d_rhs_lin, lin_size);
		cudaDeviceSynchronize();
	tridiag_linear_timer.Stop();

	// 
	tridiag_manyrhs_Timer.Start();
		cusparseDgtsv_nopivot(cusparseH, size, num_rhs,
				&d_data[0], &d_data[size], &d_data[2*size],
				d_rhs_many, size);
		cudaDeviceSynchronize();
	tridiag_manyrhs_Timer.Stop();

	
	double error = 0;
	for (int i = 0; i < lin_size; ++i)
	{
		error += abs(d_rhs_many[i]- d_rhs_lin[i]);
	}	

	cout << error << endl;


	float many_time = tridiag_manyrhs_Timer.Elapsed();
	float lin_time = tridiag_linear_timer.Elapsed();

	cout << "SpeedUp     :" << lin_time/many_time << endl;
	cout << "Many rhs    :" << many_time << " " << endl;
	cout << "Linear time :" << lin_time << " " << endl;
	
	return 0;
}