#include <iostream>

#include <cstdlib>
#include <cstdio>

#include <cusparse_v2.h>

#include "gpu_timer.h"

using namespace std;


int main(int argc, char const *argv[])
{
	int rowStart = 1000;
	int rowEnd = 250000;
	int rowStride = 1000;
	int rowCurrent = rowStart;

	float startPerc = 0.03;
	float endPerc = 0.03;
	float stride = 5;
	float currentPerc = startPerc;

	while(rowCurrent <= rowEnd){
		int rows = rowCurrent;
		currentPerc = startPerc;

		while(currentPerc <= endPerc){

			int num_rhs = (currentPerc*rows)/100;
			if(num_rhs == 0) num_rhs = 1;
			//num_rhs = atoi(argv[1]); // temporary code

			double* values, *d_values;
			double* rhs, *d_rhs;

			values = new double[3*rows];
			rhs = new double[num_rhs*rows];

			cudaMalloc((void**)&d_values, 3*rows*sizeof(double));
			cudaMalloc((void**)&d_rhs, num_rhs*rows*sizeof(double));

			for (int i = 0; i < 3*rows; ++i)
			{
				values[i] = rand()%100+2;
			}
			values[0] = 0;
			values[3*rows-1] = 0;

			for (int i = 0; i < num_rhs*rows; ++i)
			{
				rhs[i] = rand()%100 + 2;
			}

			cudaMemcpy(d_values, values, 3*rows*sizeof(double), cudaMemcpyHostToDevice);
			cudaMemcpy(d_rhs, rhs, num_rhs*rows*sizeof(double), cudaMemcpyHostToDevice);

			cusparseHandle_t cusparseH = NULL;
			cusparseCreate(&cusparseH);

			GpuTimer tridiagTimer;

			tridiagTimer.Start();
				// Calling tridiagonal GPU kernel
				cusparseDgtsv_nopivot(cusparseH, rows, num_rhs, &(d_values[0]), &(d_values[rows]), &(d_values[2*rows]), d_rhs, rows);
				cudaDeviceSynchronize();
			tridiagTimer.Stop();

			cout << tridiagTimer.Elapsed() << " ";

			delete[] values;
			delete[] rhs;

			cudaFree(d_values);
			cudaFree(d_rhs);
			cudaDeviceSynchronize();

			currentPerc += stride;

		}
		cout << endl;

		rowCurrent += rowStride;
	}

	


	return 0;
}