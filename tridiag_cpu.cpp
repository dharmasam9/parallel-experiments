#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

#include <sys/time.h>

using namespace std;

typedef unsigned long long u64;

/* get microseconds (us) */
u64 getTime()
{
 struct timeval tv;

 gettimeofday(&tv, NULL);

 u64 ret = tv.tv_usec;

 ret += (tv.tv_sec * 1000 * 1000);

 return ret;
}

// Printing matrix
void print_tridiagonal_as_matrix(double* data,int rows){
	double zero = 0;

	int zeros_left = -1;
	int zeros_right = rows-2;

	string delimeter = " , ";

	cout << "[" << endl;
	for(int i=0;i<rows;i++){

		for(int j=0;j<zeros_left;j++){
			cout << zero << delimeter;
		}

		if(i != 0)
			cout << data[i] << delimeter;

		cout << data[rows+i] << delimeter ;

		if(i != rows-1)
			cout << data[2*rows+i] << delimeter;

		for(int j=0;j<zeros_right;j++){
			cout << zero << delimeter;
		}

		zeros_left++;
		zeros_right--;

		cout << ";" << endl;
	}
	cout << "]" << endl;
}

// Coulmn major format
void print_matrix(double* data, int rows, int cols){
	cout << "[" << endl;
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			cout << data[j*rows+i] << ",";
		}
		cout << ";" << endl;
	}
	cout << "]" << endl;
}


int main(int argc, char const *argv[])
{
	srand(time(NULL));

	int rowStart = 1000;
	int rowEnd = 250000;
	int rowStride = 1000;
	int rowCurrent = rowStart;

	while(rowCurrent <= rowEnd){
		int rows = rowCurrent;

		double* values = new double[3*rows];
		double* rhs = new double[rows];
		double* x = new double[rows];

		for (int i = 0; i < 3*rows; ++i)
		{
			values[i] = (rand()%8)+2; // Any thing between 2:9
		}

		for (int i = 0; i < rows; ++i)
		{
			rhs[i] = (rand()%8)+2; // Any thing between 2:9
		}

		if(rows < 15){
			print_tridiagonal_as_matrix(values, rows);
			print_matrix(rhs, rows, 1);	
		}

		// Top of left diag and bottom of right diag set to zero
		values[0] = 0;
		values[3*rows-1] = 0;

		// -------------------- GAUSS ELIMINATION ----------------------

		u64 start_time = getTime();

			// Forward elimination
			for (int i = 1; i < rows; ++i)
			{
				values[rows+i] -= values[2*rows+i-1]*(values[i]/values[rows+i-1]);
				rhs[i] -= rhs[i-1]*(values[i]/values[rows+i-1]);
			}

			// Back Substitution
			x[rows-1] = rhs[rows-1]/values[rows+rows-1];
			for (int i = rows-2; i >=0; i--)
			{
				x[i] = (rhs[i]-values[2*rows+i]*x[i+1])/values[rows+i];
			}

		u64 end_time = getTime();

		double time_taken =  (end_time-start_time)/1000.0f;

		if(rows < 15)	
			print_matrix(x, rows, 1);

		printf("%lf\n", time_taken);

		delete[] values;
		delete[] rhs;
		delete[] x;

		rowCurrent += rowStride;
	}

	return 0;
}
